# Wikiway back-end project English version

## Table of Contents

1. [General presentation of the application](#overview-of-the-application)
2. [Wikiway Structure](#structure-of-wikiway)
3. [Technologies used and Evolution of Wikiway](#technologies-used-and-evolution-of-wikiway)
4. [Functions](#functions)
5. [Setup](#setup)
6. [Useful links](#useful-links)
7. [Authors](#authors)

### Overview of the application

Wikiway is an ios and android application allowing you to search for all the landmarks referenced on Wikidata located around you such as the Eiffel Tower, the Arc de Triomphe, and many more. What is available around me? You will have everything in the Wikiway app! Wikiway was created by Bivilink in partnership with Wikipedia.

### Structure of Wikiway

In this part, we show how Wikiway is structured by showing all the folders and files: how the folders are distributed and what they contain.

* Root

    * Folders

| Folder     | Descriptions                                    |
|------------|-------------------------------------------------|
| resources  | Contains the config.ini file                    |
| src        | Contains all wikiway folders and files          |
| api        | -                                               |
| controller | Contains all main files                         |
| model      | Contains all errors to return and regex         |
| tools      | Contains html_parser.py and request_verifier.py |

    * The files and their contents

   **requirements.txt**: Lists all dependencies used in the code.

   **main.py**: Corresponds to the file that will allow us to initialize the routes and start the application using this file.

   **pages_controller.py**: Corresponds to the API which allows you to collect the information necessary to obtain the results of the monuments around you, according to the themes.

   **html_parser.py**: Corresponds to the second API which allows to scrape (harvest) all the text of the Wikipedia page.

   **modelgeosearch.py**: This allows you to manage API regex.

   **request_verifier.py**: Used to check if the fields apart from the user correspond to what is being requested.

   **reports_controller.py**: Allows the user to send an error back to the server in POST mode.

   **errors.py**: References and allows to set up all the errors.

   **thematics_pages_controllers.docx**: References all the monuments of the application.


### Technologies used and Evolution of Wikiway

| Versions             | Technologies used and specificities                                                                      |
|----------------------|----------------------------------------------------------------------------------------------------------|
| Version 1 of wikiway | Version 1 of wikiway	Version with the listed landmarks sortable using filters and categories (“themes”). |
| Release 2            | in progress                                                                                              |


### Functions

| Important functions to know | roles                                                                             |
|-----------------------------|-----------------------------------------------------------------------------------|
| *get_geosearch()*           | Function that allows you to retrieve all the information to display the landmarks |
| *get_result()*              | returns in Json the text of a wikipedia page                                      |


### Setup

1. Install [Python 3.9](https://www.python.org/downloads/).
2. Install [PyCharm](https://www.jetbrains.com/en-us/pycharm/download/#section=windows) JetBrains Ide.
3. Create a Gitlab account.
4. Install [Git](https://git-scm.com/download/win) if needed.
5. Open Git and with the "ls" command look at your files then with the "cd" command go to the PyCharmsProjects file the path corresponds to "YourHardDisk"\Users\YourUsers\PycharmProjects.
6. Type the command "git clone https://gitlab.com/wikiway/wikiway-api.git" in the PycharmProjects folder.
7. Identify yourself (Git username and password) to create a wikiway-api folder for you.
8. Open the Pycharm IDE and then select "Open..." search for the Wikiway-api project installed on your pc beforehand.
9. Then maybe you will need to configure the python in the Pycharm IDE so for that you have to go to "File" at the top left and then settings.
10. You will normally see the section "Project: Wikiway-api" click on it and then you have to select "python interpreter".
11. Click on the gear icon and go find the python.exe file 
12. for install all the dependencies you need to type the command "py -m pip install -r requirements.txt" in the projet.
13. If you still cannot launch the server then you need to configure at the top right to the left of the play button "⏵" click on the drop-down list and do edit configuration.
14. The information that must be entered is:
    -name=main
    - script path = "YourHardDisk"\Users\YourUsers\PycharmProjects\wikiway-api\src\main.py
    - python interpreter = "YourHardDisk"\ProgramFiles\python\python.exe
15. Hit "apply" and "ok".
16. Now launch the project using the play "⏵" button and the console should launch.
17. If you want to perform a test I advise you to install [Postman](https://www.postman.com/downloads/) (allows you to test requests) and add a new GET request and put this in url
"http://127.0.0.1:5000/html_parse?article=https://fr.wikipedia.org/wiki/Cimeti%C3%A8re_ancien_de_Ch%C3%A2tenay-Malabry"
if it returns you a json it means that the server is working.
18. Well done!!! You have just made Wikiway work locally.

* useful command
"pip freeze > requirements.txt" this command is used to see all packages (dependencies) installed in the code.


### Useful links

Query of the pages_controller.py file: https://query.wikidata.org/#%23%C3%89l%C3%A9ment%20situ%C3%A9s%20autour%20des%20locals%20de%20la%20fondation%20Wikimedia%2C %20tri%C3%A9s%20par%20%C3%A9distance%0ASELECT%20%3Fplace%20%3Flocation%20%3Fdistance%20%3FplaceLabel%20WHERE%20%7B%0A%20%20%20%20SERVICE%20wikibase %3Aaround%20%7B%20%0A%20%20%20%20%20%20%3Fplace%20wdt%3AP625%20%3Flocation%20.%20%0A%20%20%20%20%20% 20bd%3AserviceParam%20wikibase%3Acenter%20%22Point%28-122.402251%2037.789246%29%22%5E%5Egeo%3AwktLiteral%20.%20%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase %3Aradius%20%221%22%20.%20%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Adistance%20%3Fdistance%20.%0A%20%20%20%20 %7D%20%0A%20%20%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22%5BAUTO_LANGUAGE%5D%2Cen%22.%20%7D%0A% 7D%20ORDER%20BY%20%3Fdistance%20LIMIT%20100
wikidata : https://www.wikidata.org/wiki/Wikidata:Main_Page
problem call app to api : https://stackoverflow.com/questions/67083014/connecting-to-localhost-with-react-native

if you need to find a params to add in the query search a monument in wikidata with this characteristic and then choose the properties in the case "instance of" in the link wikidata monument


### Authors

MAFFEI Enzo (developer full-stack) 


# Projet back-end Wikiway Version française

## Tables des matières

1. [Présentation générale de l'application](#présentation-générale-de-l'application)
2. [Structure de Wikiway](#structure-de-wikiway)
3. [Technologies utilisées et Evolution de Wikiway](#technologies-utilisées-et-evolution-de-wikiway)
4. [Fonctions](#fonctions)
5. [Installation](#installation)
6. [Liens utiles](#liens-utiles) 
7. [Auteurs](#auteurs)

### Présentation générale de l'application

Wikiway est une application ios et android permettant de rechercher touts les monuments disponibles sur le site [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) autour de soi tel que la tour Eiffel ou encore l'arc de Triomphe et plein d'autre. Qu'est-ce qui est disponible autour de moi ? Vous aurez tout dans l'application Wikiway!

### Structure de Wikiway

Dans cette partie, on montre comment Wikiway est structuré en montrant l'ensemble des dossiers et fichiers : comment se repartissent les dossiers et que contiennent ces derniers.

* Root (Racine)

    * Dossiers

| Dossier    | Descriptions                                      |
|------------|---------------------------------------------------|
| ressources | Contient le fichier config.ini                    |
| src        | Contient tous les dossiers et fichiers de wikiway |
| api        | -                                                 |
| controller | Contient tous les fichiers principaux             |
| model      | Contient toutes les erreurs à renvoyer et regex   |
| utils      | Contient html_parser.py et request_verifier.py    |
| test       | Un fichier pour les tests                         |

    * Les fichiers et ses contenus

   **requirements.txt** : Liste toutes les dépendances utilisées dans le code.

   **main.py** : Correspond au fichier qui va nous permettre d'initialiser les routes et de démarrer l'application grâce à ce fichier.  

   **pages_controller.py** : Correspond à l'api qui permet de récolter les informations nécessaires pour avoir les résultats des monuments autour de soi, en fonction des thématiques.

   **html_parser.py** : Correspond à la deuxième api qui permet de scraper (récolter) tout le text de la page Wikipedia. 

   **modelgeosearch.py** : Celui-ci permet de gérer les regex des apis.

   **request_verifier.py** : Sert à verifier si les champs mis part l'utilisateur correspondent bien à ce que l'on demande.

   **reports_controller.py** : Permet à l'utilisateur de renvoyer une erreur au serveur en mode POST.  

   **errors.py** : Référence et permet de mettre en place toutes les erreurs.     

   **thematics_pages_controllers.docx** : Référence touts les monuments de l'application.


### Technologies utilisées et Evolution de Wikiway

| Versions             | Technologies utilisées et specificités                                                 |
|----------------------|----------------------------------------------------------------------------------------|
| Version 1 de wikiway | Version avec les monuments en forme de liste et possibilité de choisir ces thématiques |
| Version 2            | en cours de réalisation                                                                |


### Fonctions

| Fonctions importantes à connaitre | roles                                                                                |
|-----------------------------------|--------------------------------------------------------------------------------------|
| *get_geosearch()*                 | Fonction qui permet de recuperer toutes les informations pour afficher les monuments |
| *get_result()*                    | retourne en Json le text d'une page de wikipedia                                     |


### Installation

1. Installer [Python 3.9](https://www.python.org/downloads/).
2. Installer [PyCharm](https://www.jetbrains.com/fr-fr/pycharm/download/#section=windows) l'Ide de JetBrains.
3. Créer un compte Gitlab.
4. Installer [Git](https://git-scm.com/download/win) si besoin.
5. Ouvrer Git et avec la commande "ls" regarder vos fichiers ensuite avec la commande "cd" aller dans le fichier PyCharmsProjects le chemin correspond à VotreDisqueDur\Users\VotreUsers\PycharmProjects.
6. Tapper la commande "git clone https://gitlab.com/wikiway/wikiway-api.git" dans le dossier PycharmProjects.
7. Identifiez-vous (nom d'utilisateur et mot passe Git) et cela va vous créer un dossier wikiway-api.
8. Ouvrer l'ide Pycharm et ensuite sélectionner "Open..." chercher le projet Wikiway-api installer au préalable dans votre pc. 
9. Ensuite peut-être que vous aurez besoin de configurer le python dans l'ide Pycharm alors pour cela il faut aller dans "File" en haut à gauche et ensuite settings.
10. Vous verrez normalement la section "Project: Wikiway-api" cliquez dessus et ensuite il faut sélectionner "python interpreter".
11. Cliquez sur l'engrenage et allez chercher le fichier python.exe c'est le chemin où vous avez décider d'installer Python.
12. Pour installer toutes les dependances vous auriez besoin de faire la commande "py -m pip install -r requirements.txt" dans le projet.
13. Si vous ne pouvez toujours pas lancer le serveur alors il faut que vous configurer en haut à droite à la gauche du bouton play "⏵" cliquer sur la liste déroulante et faîtes edit configuration.
14. Les informations qu'il faut rentrer sont: 
    - name = main  
    - script path = VotreDisqueDur\Users\VotreUsers\PycharmProjects\wikiway-api\src\main.py
    - python interpreter = VotreDisqueDur\ProgramFiles\python\python.exe
15. Faites "apply" et "ok".
16. Maintenant, lancez le projet grâce au bouton play "⏵" et la console devrait se lancer.
17. Si vous voulez effectuer un test je vous conseille d'installer [Postman](https://www.postman.com/downloads/) (permet de tester les requètes) et d'ajouter une nouvelle requète GET et de mettre ceci en url 
"http://127.0.0.1:5000/html_parse?article=https://fr.wikipedia.org/wiki/Cimeti%C3%A8re_ancien_de_Ch%C3%A2tenay-Malabry"
si cela vous renvoie un json cela veut dire que le serveur fonctionne.
18. Bravo !!! Vous venez de faire marcher Wikiway en local.

* commande utile
"pip freeze > requirements.txt" cette commande sert à voir tous les packages (dépendances) installer dans le code.


### Liens utiles

Query du fichier pages_controller.py: https://query.wikidata.org/#SELECT%20%2a%20%7B%7B%0ASELECT%20%3Fplace%20%3Flocation%20%3Fdistance%20%3FplaceLabel%20%3Fperson%20%3FpersonLabel%20%3Fimage%20%3Farticle%20WHERE%20%7B%0A%20%20%20%20SERVICE%20wikibase%3Aaround%20%7B%20%0A%20%20%20%20%20%20%3Fplace%20wdt%3AP625%20%3Flocation%20.%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Acenter%20%22Point%28-122.402251%2037.789246%29%22%5E%5Egeo%3AwktLiteral%20.%20%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Aradius%20%2215%22%20.%20%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Adistance%20%3Fdistance%20.%0A%20%20%20%20%7D%20%0A%20%20%20%20%7B%3Fplace%20wdt%3AP31%20wd%3AQ515%7D%0A%20%20%3Farticle%20schema%3Aabout%20%3Fplace%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fen.wikipedia.org%2F%3E%20.%0A%20%20%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%20%7D%0A%7D%7D%0AUNION%20%7B%0ASELECT%20%3Flocation%20%3Fdistance%20%3Fperson%20%3FpersonLabel%20%3Fimage%20%3Farticle%20WHERE%20%7B%0A%20%20%20%20SERVICE%20wikibase%3Aaround%20%7B%20%0A%20%20%20%20%20%20%3Fplace%20wdt%3AP625%20%3Flocation%20.%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Acenter%20%22Point%28-122.402251%2037.789246%29%22%5E%5Egeo%3AwktLiteral%20.%20%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Aradius%20%221%22%20.%20%0A%20%20%20%20%20%20bd%3AserviceParam%20wikibase%3Adistance%20%3Fdistance%20.%0A%20%20%20%20%7D%20%0A%20%20%3Fperson%20wdt%3AP19%20%3Fplace%20.%0A%20%20%3Farticle%20schema%3Aabout%20%3Fperson%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20schema%3AisPartOf%20%3Chttps%3A%2F%2Fen.wikipedia.org%2F%3E%20.%0A%20%20%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%20%7D%0A%20%20OPTIONAL%7B%3Fperson%20wdt%3AP18%20%3Fimage%7D%0A%7D%7D%0A%7D%0AORDER%20BY%20%3Fdistance%20LIMIT%20100%20%20
wikidata : https://www.wikidata.org/wiki/Wikidata:Main_Page
problème appel app vers api : https://stackoverflow.com/questions/67083014/connecting-to-localhost-with-react-native

Si vous avez besoin de trouver un paramètre à ajouter dans la requête recherchez un monument avec cette caractéristique dans wikidata puis choisissez les propriétés dans le cas "instance of" dans le lien wikidata monument


### Auteurs

MAFFEI Enzo (developpeur full-stack)