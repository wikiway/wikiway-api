import requests
import time
from flask import request, jsonify
from bs4 import BeautifulSoup
from model.modelgeosearch import PARSE_QUERY
from utils.request_verifier import verify_query


class HtmlParser:
    """function HtmlParser: scrap text from the url page wikipedia
    return : the text from the url page wikipedia
    """

    """function : scrap the title"""

    def get_title(self):
        soup = self.soup
        per = soup.h1
        title = per.text
        return title

    """ function : for search the infobox props """

    # infobox ib-settlement vcard
    def infobox(self):

        soup = self.soup
        if soup.find(class_="infobox infobox_v2"):
            return soup.find(class_="infobox infobox_v2")
        elif soup.find(class_="infobox_v2 noarchive"):
            return soup.find(class_="infobox_v2 noarchive")
        elif soup.find(class_="infobox infobox_v3 large"):
            return soup.find(class_="infobox infobox_v3 large")
        elif soup.find(class_="infobox_v3 noarchive large"):
            return soup.find(class_="infobox_v3 noarchive large")
        elif soup.find(class_="mw-body-header"):
            return soup.find(class_="mw-body-header")
        elif soup.find(class_="infobox infobox_v3"):
            return soup.find(class_="infobox infobox_v3")
        elif soup.find(class_="infobox vcard"):
            return soup.find(class_="infobox vcard")
        elif soup.find(class_="infobox ib-settlement vcard"):
            return soup.find(class_="infobox ib-settlement vcard")
        elif soup.find(class_="thumb tright"):
            return soup.find(class_="thumb tright")
        elif soup.find(class_="infobox ib-settlement vcard"):
            return soup.find(class_="infobox ib-settlement vcard")
        elif soup.find(class_="infobox ib-islands vcard"):
            return soup.find(class_="infobox ib-islands vcard")
        elif soup.find(class_="mw-parser-output"):
            return soup.find(class_="mw-parser-output")

    """ function : scrap the introduction on page wikipedia """

    def get_introduction(self):

        soup = self.soup
        per = HtmlParser.infobox(self)
        next_td_tag = per.find_next_sibling()
        next = next_td_tag.previous_sibling
        first_child = next.find_all_next()  # find all others tag in the html code
        intro = []

        try:
            # for each tag in first_child :
            for balise in first_child:

                # if find a h2 tag break the boucle
                if balise.find_all(class_='geo-default'):
                    balise.decompose()
                if balise.name == 'ul':
                    for i in balise.find_all(class_="gallery mw-gallery-traditional"):
                        i.decompose()
                if balise.name == 'p':
                    bal = balise.text
                    bal1 = bal.replace("\n", "")  # ici faire enlever les #n
                    intro.append({"text0": bal1})
                # if find a supp tag decompose it
                for sup_tag in balise.find_all('sup'):
                    sup_tag.decompose()

                if balise.name == 'h2':
                    break
                # intro will append tag for each tag p
                # delete "\n" parasite in the text

                # delete this ligne

            return intro
        except:
            return None

    # this is an exemple for the title Typonomie in the page wikipedia
    """def get_Typonomie(self):

        soup = self.soup
        try:

            per = soup.find(id="Typonomie")
            first = per.find_all_next()
            text = ''
            item = ["item"]

            for p in first[8:]:
                if p.find_parent("table"):
                    continue
                if p.find_parent(class_="gallery mw-gallery-packed  "):
                    continue
                if p.find_parent("h2"):
                    break
                if p.name != 'p':
                    continue
                for sup_tag in p.find_all('sup'):
                    sup_tag.decompose()
                text = p.text
                i = text.replace('\n', '')
                item.append(i)

            return item
        except:
            return None"""

    """function for get all text from page wikipedia except the introduction and the title"""

    def get_all(self):
        soup = self.soup
        # we need a try and catch if the heading is empty

        try:

            # we find a tag h2 with this tag
            per = soup.find(class_="mw-headline")

            # create a dict
            item = ["item"]
            i = 0
            # item append the first title because the first title is not in the boucle for
            item.append({"title1": per.text})

            # for i in all tag h2
            for i in soup.find_all('h2'):

                # for sib in all next tag i
                for sib in i.next_siblings:

                    # if sib.name == p so keep the text in the dict item and append for each tag p
                    if sib.name == 'p':

                        sib2 = sib.text
                        ir = sib2.replace('\n', "")
                        item.append({"text1": ir})

                        for sup_tag in sib.find_all('sup'):
                            sup_tag.decompose()

                    # faire une fonction pour scrapper les P et faire un else qui dirait que si il n'y a pas alors renvoyer un text*

                    if sib.name == 'div':
                        if sib.find(class_='bandeau-cell bandeau-icone-css incomplet'):
                            item.append({
                                "text1": "Cette section est vide, insuffisamment détaillée ou incomplète. Votre aide est la bienvenue! Vous pouvez nous aider en cliquant sur le crayon avec un carré en haut à droite"})

                    """if sib.name == "h3":
                        item.append({"title1": sib.text})

                        for s in sib.find_all(class_="mw-editsection"):
                            s.decompose()"""

                    if sib.name == "ul":
                        for i in sib.find_all('li', {"class": "gallerybox"}):
                            i.decompose()
                        for i in sib.find_all('span', {"class": "bandeau-portail-element"}):
                            i.decompose()

                        for li in sib:

                            if li == "\n":
                                li.extract()

                            sib2 = li.text
                            rt = sib2.strip("\n")

                            item.append({"text1": rt})
                            # corriger parce que les listes vides creer des espaces en trop sur l'appli
                            # supp the balise tag 'sup' bcs the tag show parasite number in the paragraphe

                            for sup_tag in sib.find_all('sup'):
                                sup_tag.decompose()

                    # if the tag == h2
                    if sib.name == 'h2':

                        # delete this ligne

                        # all if help to delete title which did not text in the h2 tag

                        if sib.find(id='Notes_et_références'):
                            sib.extract()
                            break
                        if sib.find(id='Références'):
                            sib.extract()
                            break
                        if sib.find(id='References'):
                            sib.extract()
                            break
                        if sib.find(id='Notes'):
                            sib.extract()
                            break
                        if sib.find(id='Pour_approfondir'):
                            sib.extract()
                            break
                        if sib.find(id='Anecdotes'):
                            sib.extract()
                            break
                        if sib.find(id='Voir_aussi'):
                            sib.extract()
                            break
                        if sib.find(id='Sources'):
                            sib.extract()
                            break
                        if sib.find(id='Liens_externes'):
                            sib.extract()
                            break
                        if sib.find(id='Liens_externe'):
                            sib.extract()
                            break
                        if sib.find(id='External_links'):
                            sib.extract()
                            break
                        if sib.find(id='Bibliographie'):
                            sib.extract()
                            break
                        if sib.find(id='Galerie'):
                            sib.extract()
                            break
                        if sib.find(id='Annexes'):
                            sib.extract()
                            break
                        for s in sib.find_all(class_="mw-editsection"):
                            s.extract()
                        # others title scrap on the dict
                        item.append({"title1": sib.text})
                        break
            return item

        except:
            return None

    """function for show all results and the params needed for choice a url page wikipedia """

    def get_result(self):
        # article is a url page wikipedia a exemple : https://fr.wikipedia.org/wiki/Tour_Eiffel
        url = request.args.get('article')

        rsp = requests.get(url)
        self.soup = BeautifulSoup(rsp.text, "html.parser")

        # for verify if the params is true
        global params
        validation = verify_query(request.args, PARSE_QUERY)
        if validation is not None:
            return validation

        itemNothing = ["item"]

        itemNothing.append({"title1": None})
        itemNothing.append({"text1": None})

        itemIntro = ["item"]
        itemNothing.append({"text0": None})
        # api return this

        item = [{
            'title': HtmlParser.get_title(self) if HtmlParser.get_title(self) else None,
            'introduction': HtmlParser.get_introduction(self) if HtmlParser.get_introduction(self) else itemIntro,
            'all': HtmlParser.get_all(self) if HtmlParser.get_all(self) else itemNothing
        }]

        return jsonify(item)
