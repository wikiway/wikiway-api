from flask import json, Response

MIME_TYPE = "application/json"


class Error:

    def __init__(self, error_code: str, message: str, status: int):
        self.error_code = error_code
        self.message = message
        self.status = status


def error_builder(list: list[Error]) -> Response:
    """get the response to send when an error is multiple

        :error_code: the error_code in the error invalid_param and missing param
        :message: the message in the error invalid_param and missing param
        :return: the response to send back
        """
    res = {
        "errors": []
    }
    max_status = 0

    for item in list:
        error_dict = {
            "error_code": item.error_code,
            "message": item.message
        }

        if item.status > max_status:
            max_status = item.status

        res["errors"].append(error_dict)

    return Response(
        response=json.dumps(res),
        status=max_status,
        mimetype=MIME_TYPE
    )


def invalid_param(field: str, regex: str) -> Error:
    """get the response to send when an invalid query parameter is given

    :param field: the field name that is invalid
    :param regex: the regex that must match the field
    :return: the response to send back
    """
    return Error(
        error_code="INVALID_PARAMETER",
        message="The request has an invalid query parameter : " + field + ". This field must match the following "
                                                                          "regex : " + regex,
        status=400 #400
    )


def missing_param(field: str) -> Error:
    """get the response to send when there is a missing query parameter

    :param field: the field name that is missing
    :return: the response to send back
    """
    return Error(
        error_code="MISSING_PARAMETER",
        message="The request is missing the required query parameter " + field,
        status=400 #400
    )


def invalid_field(field: str, regex: str) -> Error:
    """get the response to send when an invalid query parameter is given

    :param field: the field name that is invalid
    :param regex: the regex that must match the field
    :return: the response to send back
    """
    return Error(
        error_code="INVALID_FIELD",
        message="The request has an invalid query parameter : " + field + ". This field must match the following "
                                                                          "regex : " + regex,
        status=400 #400
    )


def missing_field(field: str) -> Error:
    """get the response to send when there is a missing query parameter

    :param field: the field name that is missing
    :return: the response to send back
    """
    return Error(
        error_code="MISSING_FIELD",
        message="The request is missing the required query parameter " + field,
        status=400 #400
    )


def invalid_json() -> Response:
    """get the response to send when the JSON of request is invalid

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "errors":
                {
                    "error_code": "INVALID_JSON",
                    "message": "The provided JSON is not valid."
                }
        }),
        status=400,#400
        mimetype=MIME_TYPE
    )


def not_found() -> Response:
    """get the response to send when the given endpoint is unknown

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "errors":
                {
                    "error_code": "NOT_FOUND",
                    "message": "No resource found"
                }

        }),
        status=404,#404
        mimetype=MIME_TYPE
    )


def internal_error() -> Response:
    """get the response to send when an unexpected error occured

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "errors":
                {
                    "error_code": "INTERNAL_SERVER_ERROR",
                    "message": "An unexpected error occurred."
                }

        }),
        status=500,#500
        mimetype=MIME_TYPE
    )


def method_not_allowed() -> Response:
    """get the response to send when a request is not correct for a endpoints

    :return: the response to send back
    """
    return Response(
        response=json.dumps({
            "errors":
                {
                    "error_code": "METHOD_NOT_ALLOWED",
                    "message": "The HTTP method you used is not allowed for that endpoint."
                }

        }),
        status=405,#405
        mimetype=MIME_TYPE
    )
