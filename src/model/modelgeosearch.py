"""the class modelgeosearch is a model,
who confirm if the params in the request is true or false"""

# the allowed thematics for page pages_controller.py
ALLOWED_SCOPES = "transport_zone|Natural_spaces|Leisure_and_relaxation_areas|Places_of_culture_and_history" \
                 "|commercial_zone_and_industry|place_of_power|city|etudes_sante|Lieux_events|lieux_culte" \
                 "|place_and_street|celebrite|archeologie|eau|"  # allowed scopes
SCOPES_NUMBER = len(ALLOWED_SCOPES.split('|'))

# the regex for reports_controller.py
REPORTS_QUERY = \
    [
        {
            'name': 'title',
            'required': True,
            'regex': r".{0,100}"
        },
        {
            'name': 'message',
            'required': True,
            'regex': r".{0,2000}"
        },
        {
            'name': 'date_heure',
            'required': True,
            'regex': r".{0,50}"
        },
        {
            'name': 'url',
            'required': True,
            'regex': r"(.+)"
        }
    ]

# the regex for pages_controller.py
GEOSEARCH_QUERY = \
    [
        {
            'name': 'latitude',
            'required': True,
            'regex': r"-?[0-9]+\.[0-9]+"
        },
        {
            'name': 'longitude',
            'required': True,
            'regex': r"-?[0-9]+\.[0-9]+"
        },
        {
            'name': 'radius',
            'required': True,
            'regex': r"1|5|10|30|50|100|200|500"
        },
        {
            'name': 'thematics',
            'required': False,
            'regex': r"((" + ALLOWED_SCOPES + ") ){0," + str(SCOPES_NUMBER - 1) + "}(" + ALLOWED_SCOPES + "){1}"
        }
    ]

# the regex for html_parser.py
PARSE_QUERY = \
    [
        {
            'name': 'article',
            'required': True,
            'regex': r"(.+)"
        },
    ]