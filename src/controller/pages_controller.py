import sys
import requests
from SPARQLWrapper import SPARQLWrapper, JSON
from bs4 import BeautifulSoup
from flask import request, jsonify
from model.modelgeosearch import GEOSEARCH_QUERY
from utils.request_verifier import verify_query


class Geosearch:
    """class GeosearchController: class that will search latitude, longitude and radius and create a page for a user
    (get_ID) return to client : latitude, longitude, radius, languages, pages and thematics(list_dict)
    """

    def get_geosearch(self):

        # verify if the params is true
        global params
        validation = verify_query(request.args, GEOSEARCH_QUERY)
        if validation is not None:
            return validation

        # params needed
        latitude = request.args.get('latitude')
        radius = request.args.get('radius')
        longitude = request.args.get('longitude')
        languages = request.args.get('languages')
        pages = request.args.get('pages')
        list_dict = request.args.get('thematics')
        celebrite = request.args.get('celebrite')

        # thematics dict for wikidata (look the thematics_pages_controllers.docx on gitlab for more info)
        a = {
            'city': """
                                { ?place wdt:P31 wd:Q702842.}
                                UNION
                                { ?place wdt:P31 wd:Q5119.}
                                UNION
                                { ?place wdt:P31 wd:Q1050303.}
                                UNION
                                { ?place wdt:P31 wd:Q484170.}
                                UNION
                                { ?place wdt:P31 wd:Q6465.}
                                UNION
                                { ?place wdt:P31 wd:Q133346.}
                                UNION
                                { ?place wdt:P31 wd:Q5084.}
                                UNION
                                { ?place wdt:P31 wd:Q6256.}
                                UNION
                                { ?place wdt:P31 wd:Q2221906.}
                                UNION
                                { ?place wdt:P31 wd:Q123705.}
                                UNION
                                { ?place wdt:P31 wd:Q36784.}
                                UNION
                                { ?place wdt:P31 wd:Q82794.}
                                UNION
                                { ?place wdt:P31 wd:Q1620908.}
                                UNION
                                { ?place wdt:P31 wd:Q572995.}
                                UNION
                                { ?place wdt:P31 wd:Q532.}
                                UNION
                                { ?place wdt:P31 wd:Q515.}
                                UNION
                                { ?place wdt:P31 wd:Q174844.}
                                UNION
                                { ?place wdt:P31 wd:Q83620.}""",

            'place_of_power': """
                                { ?place wdt:P31 wd:Q3917681.}
                                UNION
                                { ?place wdt:P31 wd:Q1752346.}
                                UNION
                                { ?place wdt:P31 wd:Q481289.}
                                 UNION
                                { ?place wdt:P31 wd:Q189445.}
                                UNION
                                { ?place wdt:P31 wd:Q131263.}
                                UNION
                                { ?place wdt:P31 wd:Q83554028.}
                                UNION
                                { ?place wdt:P31 wd:Q190752.}
                                UNION
                                { ?place wdt:P31 wd:Q14037025.}
                                UNION
                                { ?place wdt:P31 wd:Q178706.}
                                UNION
                                { ?place wdt:P31 wd:Q25550691.}
                                UNION
                                { ?place wdt:P31 wd:Q192350.}
                                UNION
                                { ?place wdt:P31 wd:Q15284.}
                                UNION
                                { ?place wdt:P31 wd:Q22575704.}
                                UNION
                                { ?place wdt:P31 wd:Q13220204.}
                                UNION
                                { ?place wdt:P31 wd:Q41487.}
                                UNION
                                { ?place wdt:P31 wd:Q192611.}
                                UNION
                                { ?place wdt:P31 wd:Q637846.}
                                UNION
                                { ?place wdt:P31 wd:Q375928.}
                                UNION
                                { ?place wdt:P31 wd:Q36784.}
                                UNION
                                { ?place wdt:P31 wd:Q6465.}
                                UNION
                                { ?place wdt:P31 wd:Q41487.}
                                UNION
                                { ?place wdt:P31 wd:Q6852233.}
                                UNION
                                { ?place wdt:P31 wd:Q52177058.}
                                UNION
                                { ?place wdt:P31 wd:Q481289.}
                                UNION
                                { ?place wdt:P31 wd:Q1198413.}
                                UNION
                                { ?place wdt:P31 wd:Q245016.}""",

            'commercial_zone_and_industry': """
                                { ?place wdt:P31 wd:Q245016.}
                                UNION
                                { ?place wdt:P31 wd:Q1133961.}
                                UNION
                                { ?place wdt:P31 wd:Q134447.}
                                UNION
                                { ?place wdt:P31 wd:Q159719.}
                                UNION
                                { ?place wdt:P31 wd:Q216107.}
                                UNION
                                { ?place wdt:P31 wd:Q218739.}
                                UNION
                                { ?place wdt:P31 wd:Q22687.}
                                UNION
                                { ?place wdt:P31 wd:Q380342.}
                                UNION
                                { ?place wdt:P31 wd:Q4387609.}
                                UNION
                                { ?place wdt:P31 wd:Q4830453.}
                                UNION
                                { ?place wdt:P31 wd:Q6881511.}
                                UNION
                                { ?place wdt:P31 wd:Q738570.}
                                UNION
                                { ?place wdt:P31 wd:Q7540126.}
                                UNION
                                { ?place wdt:P31 wd:Q83405.}
                                UNION
                                { ?place wdt:P31 wd:Q330284.}
                                UNION
                                { ?place wdt:P31 wd:Q180846.}""",

            'Places_of_culture_and_history': """
                                { ?place wdt:P31 wd:Q7075.}
                                UNION
                                { ?place wdt:P31 wd:Q15090615.}
                                UNION
                                { ?place wdt:P31 wd:Q18123390.}
                                UNION
                                { ?place wdt:P31 wd:Q464980.}
                                UNION
                                { ?place wdt:P31 wd:Q3152824.}
                                UNION
                                { ?place wdt:P31 wd:Q4989906.}
                                UNION
                                { ?place wdt:P31 wd:Q33506.}
                                UNION
                                { ?place wdt:P31 wd:Q219423.}
                                UNION
                                { ?place wdt:P31 wd:Q153562.}
                                UNION
                                { ?place wdt:P31 wd:Q838948.}
                                UNION
                                { ?place wdt:P31 wd:Q18674739.}
                                UNION
                                { ?place wdt:P31 wd:Q1763828.}
                                UNION
                                { ?place wdt:P31 wd:Q41253.}
                                UNION
                                { ?place wdt:P31 wd:Q179700.}
                                UNION
                                { ?place wdt:P31 wd:Q24354.}
                                UNION
                                { ?place wdt:P31 wd:Q16560.}
                                UNION
                                { ?place wdt:P31 wd:Q1060829.}
                                UNION
                                { ?place wdt:P31 wd:Q170980.}
                                UNION
                                { ?place wdt:P31 wd:Q3152824.}
                                UNION
                                { ?place wdt:P31 wd:Q3476533.}
                                UNION
                                { ?place wdt:P31 wd:Q860861.}""",

            'Leisure_and_relaxation_areas': """
                                { ?place wdt:P31 wd:Q1144661.}
                                UNION
                                { ?place wdt:P31 wd:Q2281788.}
                                UNION
                                { ?place wdt:P31 wd:Q2063507.}
                                UNION
                                { ?place wdt:P31 wd:Q30022.}
                                UNION
                                { ?place wdt:P31 wd:Q133215.}
                                UNION
                                { ?place wdt:P31 wd:Q1621657.}
                                UNION
                                { ?place wdt:P31 wd:Q15848826.}
                                UNION 
                                { ?place wdt:P31 wd:Q847017.}
                                UNION
                                { ?place wdt:P31 wd:Q13406554.}
                                UNION
                                { ?place wdt:P31 wd:Q1228895.}
                                UNION
                                { ?place wdt:P31 wd:Q27686.}
                                UNION
                                { ?place wdt:P31 wd:Q132241.}
                                UNION
                                { ?place wdt:P31 wd:Q288514.}
                                UNION
                                { ?place wdt:P31 wd:Q14092.}
                                UNION
                                { ?place wdt:P31 wd:Q11822917.}
                                UNION
                                { ?place wdt:P31 wd:Q1076486.}
                                UNION
                                { ?place wdt:P31 wd:Q18608583.}
                                UNION
                                { ?place wdt:P31 wd:Q622425.}
                                UNION
                                { ?place wdt:P31 wd:Q194195.}
                                UNION
                                { ?place wdt:P31 wd:Q43501.}
                                UNION
                                { ?place wdt:P31 wd:Q1501.}
                                UNION
                                { ?place wdt:P31 wd:Q721207.}
                                UNION
                                { ?place wdt:P31 wd:Q11707.}
                                UNION
                                { ?place wdt:P31 wd:Q57305.}
                                UNION
                                { ?place wdt:P31 wd:Q909906.}
                                UNION
                                { ?place wdt:P31 wd:Q1076486.}
                                UNION
                                { ?place wdt:P31 wd:Q483110.}
                                UNION
                                { ?place wdt:P31 wd:Q2416723.}
                                UNION
                                { ?place wdt:P31 wd:Q1048525.}
                                UNION
                                { ?place wdt:P31 wd:Q570116.}
                                UNION
                                { ?place wdt:P31 wd:Q187456.}
                                UNION
                                { ?place wdt:P31 wd:Q212198.}""",

            'Natural_spaces': """
                                { ?place wdt:P31 wd:Q811534.}
                                UNION
                                { ?place wdt:P31 wd:Q6554910.}
                                UNION
                                { ?place wdt:P31 wd:Q54050.}
                                UNION
                                { ?place wdt:P31 wd:Q4421.}
                                UNION
                                { ?place wdt:P31 wd:Q199403.}
                                UNION
                                { ?place wdt:P31 wd:Q22746.}
                                UNION
                                { ?place wdt:P31 wd:Q8502.}
                                UNION
                                { ?place wdt:P31 wd:Q22698.}
                                UNION
                                { ?place wdt:P31 wd:Q8514.}
                                UNION
                                { ?place wdt:P31 wd:Q1006733.}
                                UNION
                                { ?place wdt:P31 wd:Q1136963.}
                                UNION
                                { ?place wdt:P31 wd:Q2231510.}
                                UNION
                                { ?place wdt:P31 wd:Q93352.}
                                UNION
                                { ?place wdt:P31 wd:Q184368.}
                                UNION
                                { ?place wdt:P31 wd:Q150784.}
                                UNION
                                { ?place wdt:P31 wd:Q17018380.}
                                UNION
                                { ?place wdt:P31 wd:Q7321974.}
                                UNION
                                { ?place wdt:P31 wd:Q46831.}
                                UNION
                                { ?place wdt:P31 wd:Q159675.}
                                UNION
                                { ?place wdt:P31 wd:Q207326.}
                                UNION
                                { ?place wdt:P31 wd:Q158454.}
                                UNION
                                { ?place wdt:P31 wd:Q167346.}
                                UNION
                                { ?place wdt:P31 wd:Q1266922.}
                                UNION
                                { ?place wdt:P31 wd:Q631305.}
                                UNION
                                { ?place wdt:P31 wd:Q2042028.}
                                UNION
                                { ?place wdt:P31 wd:Q184358.}
                                UNION
                                { ?place wdt:P31 wd:Q1818761.}
                                UNION
                                { ?place wdt:P31 wd:Q15089606.}
                                UNION
                                { ?place wdt:P31 wd:Q747957.}
                                UNION
                                { ?place wdt:P31 wd:Q1130322.}
                                UNION
                                { ?place wdt:P31 wd:Q55818.}
                                UNION
                                { ?place wdt:P31 wd:Q1139493.}
                                UNION
                                { ?place wdt:P31 wd:Q336764.}
                                UNION
                                { ?place wdt:P31 wd:Q187223.}
                                UNION
                                { ?place wdt:P31 wd:Q271669.}
                                UNION
                                { ?place wdt:P31 wd:Q4895393.}
                                UNION
                                { ?place wdt:P31 wd:Q107425.}
                                UNION
                                { ?place wdt:P31 wd:Q46169.}
                                UNION
                                { ?place wdt:P31 wd:Q22698.}
                                UNION
                                { ?place wdt:P31 wd:Q30121.}
                                UNION
                                { ?place wdt:P31 wd:Q34763.}
                                UNION
                                { ?place wdt:P31 wd:Q75520.}
                                UNION
                                { ?place wdt:P31 wd:Q106259.}
                                UNION
                                { ?place wdt:P31 wd:Q946033.}
                                UNION
                                { ?place wdt:P31 wd:Q1424609.}
                                UNION
                                { ?place wdt:P31 wd:Q1245089.}
                                UNION
                                { ?place wdt:P31 wd:Q473972.}
                                UNION
                                { ?place wdt:P31 wd:Q39816.}""",

            'transport_zone': """
                                { ?place wdt:P31 wd:Q1248784.}
                                UNION
                                { ?place wdt:P31 wd:Q2376564.}
                                UNION
                                { ?place wdt:P31 wd:Q105731.}
                                UNION
                                { ?place wdt:P31 wd:Q55488.}
                                UNION
                                { ?place wdt:P31 wd:Q82117.}
                                UNION
                                { ?place wdt:P31 wd:Q12280.}
                                UNION
                                { ?place wdt:P31 wd:Q12819564.}
                                UNION
                                { ?place wdt:P31 wd:Q498002.}
                                UNION
                                { ?place wdt:P31 wd:Q1055465.}
                                UNION
                                { ?place wdt:P31 wd:Q537127.}
                                UNION
                                { ?place wdt:P31 wd:Q44377.}
                                UNION
                                { ?place wdt:P31 wd:Q928830.}
                                UNION
                                { ?place wdt:P31 wd:Q2175765.}
                                UNION
                                { ?place wdt:P31 wd:Q953806.}
                                UNION
                                { ?place wdt:P31 wd:Q6501349.}
                                UNION
                                { ?place wdt:P31 wd:Q205495.}
                                UNION
                                { ?place wdt:P31 wd:Q46622.}
                                UNION
                                { ?place wdt:P31 wd:Q892168.}
                                UNION
                                { ?place wdt:P31 wd:Q285783.}""",

            'etudes_sante': """
                                { ?place wdt:P31 wd:Q327333.} 
                                UNION
                                { ?place wdt:P31 wd:Q38723.}
                                UNION
                                { ?place wdt:P31 wd:Q159334.}
                                UNION
                                { ?place wdt:P31 wd:Q2385804.}
                                UNION
                                { ?place wdt:P31 wd:Q1021290.}
                                UNION
                                { ?place wdt:P31 wd:Q184644.}
                                UNION
                                { ?place wdt:P31 wd:Q31855.}
                                UNION
                                { ?place wdt:P31 wd:Q16917.}
                                UNION
                                { ?place wdt:P31 wd:Q917182.}""",
            'Lieux_events': """
                                { ?place wdt:P31 wd:Q811979.} 
                                UNION
                                { ?place wdt:P31 wd:Q744913.}
                                UNION
                                { ?place wdt:P31 wd:Q178561.}
                                UNION
                                { ?place wdt:P31 wd:Q23413.}
                                 UNION
                                { ?place wdt:P31 wd:Q721747.}
                                UNION
                                { ?place wdt:P31 wd:Q168983.}
                                UNION
                                { ?place wdt:P31 wd:Q3839081.}
                                UNION
                                { ?place wdt:P31 wd:Q7944.}
                                UNION
                                { ?place wdt:P31 wd:Q358.}
                                UNION
                                { ?place wdt:P31 wd:Q66108498.}
                                UNION
                                { ?place wdt:P31 wd:Q9259.}
                                UNION
                                { ?place wdt:P31 wd:Q8068.}
                                UNION
                                { ?place wdt:P31 wd:Q1785071.}
                                UNION
                                { ?place wdt:P31 wd:Q57821.}
                                UNION
                                { ?place wdt:P31 wd:Q81917.}
                                UNION
                                { ?place wdt:P31 wd:Q91165.}
                                UNION
                                { ?place wdt:P31 wd:Q3199915.}
                                UNION
                                { ?place wdt:P31 wd:Q4989906.}
                                UNION
                                { ?place wdt:P31 wd:Q2223653.}
                                UNION
                                { ?place wdt:P31 wd:Q12518.}
                                UNION
                                { ?place wdt:P31 wd:Q1078765.}""",
            'lieux_culte': """
                                { ?place wdt:P31 wd:Q3146899.}
                                UNION
                                { ?place wdt:P31 wd:Q5393308.}
                                UNION
                                { ?place wdt:P31 wd:Q16970.}
                                UNION
                                { ?place wdt:P31 wd:Q39614.}
                                UNION
                                { ?place wdt:P31 wd:Q21554881.}
                                UNION
                                { ?place wdt:P31 wd:Q846659.}
                                UNION
                                { ?place wdt:P31 wd:Q2404995.}
                                UNION
                                { ?place wdt:P31 wd:Q1707610.}
                                UNION
                                { ?place wdt:P31 wd:Q1241568.}
                                UNION
                                { ?place wdt:P31 wd:Q1128397.}
                                UNION
                                { ?place wdt:P31 wd:Q108325.}
                                UNION
                                { ?place wdt:P31 wd:Q157570.}
                                UNION
                                { ?place wdt:P31 wd:Q1370598.}
                                UNION
                                { ?place wdt:P31 wd:Q842402.}
                                UNION
                                { ?place wdt:P31 wd:Q162875.}
                                UNION
                                { ?place wdt:P31 wd:Q5003624.}
                                UNION
                                { ?place wdt:P31 wd:Q44613.}
                                UNION
                                { ?place wdt:P31 wd:Q575759.}
                                UNION
                                { ?place wdt:P31 wd:Q32815.}
                                UNION
                                { ?place wdt:P31 wd:Q55876909.}
                                UNION
                                { ?place wdt:P31 wd:Q200141.}
                                UNION
                                { ?place wdt:P31 wd:Q34627.}
                                UNION
                                { ?place wdt:P31 wd:Q2087181.}
                                UNION
                                { ?place wdt:P31 wd:Q44539.}""",

            'place_and_street': """
                                { ?place wdt:P31 wd:Q79007.}
                                UNION
                                { ?place wdt:P31 wd:Q787113.}
                                UNION
                                { ?place wdt:P31 wd:Q88372.}
                                UNION
                                { ?place wdt:P31 wd:Q483453.}
                                UNION
                                { ?place wdt:P31 wd:Q34442.}
                                UNION
                                { ?place wdt:P31 wd:Q5004679.}
                                UNION
                                { ?place wdt:P31 wd:Q628179.}
                                UNION
                                { ?place wdt:P31 wd:Q174782.}
                                UNION
                                { ?place wdt:P31 wd:Q7543083.}
                                UNION
                                { ?place wdt:P31 wd:Q6017969.}
                                UNION
                                { ?place wdt:P31 wd:Q41363.}
                                UNION
                                { ?place wdt:P31 wd:Q3393392.}
                                UNION
                                { ?place wdt:P31 wd:Q54114.}""",

            'archeologie': """
                                { ?place wdt:P31 wd:Q172896.}
                                UNION
                                { ?place wdt:P31 wd:Q188040.}
                                UNION
                                { ?place wdt:P31 wd:Q35509.}
                                UNION
                                { ?place wdt:P31 wd:Q204324.}
                                UNION
                                { ?place wdt:P31 wd:Q8072.}
                                UNION
                                { ?place wdt:P31 wd:Q839954.}
                                UNION
                                { ?place wdt:P31 wd:Q11269813.}
                                UNION
                                { ?place wdt:P31 wd:Q109391.}
                                UNION
                                { ?place wdt:P31 wd:Q101659.}
                                UNION
                                { ?place wdt:P31 wd:Q164240.}
                                UNION
                                { ?place wdt:P31 wd:Q193475.}
                                UNION
                                { ?place wdt:P31 wd:Q1435994.}
                                UNION
                                { ?place wdt:P31 wd:Q56320584.}
                                UNION
                                { ?place wdt:P31 wd:Q34023.}
                                UNION
                                { ?place wdt:P31 wd:Q188734.}
                                UNION
                                { ?place wdt:P31 wd:Q12146012.}""",

            'eau': """
                                { ?place wdt:P31 wd:Q1322134.}
                                UNION
                                { ?place wdt:P31 wd:Q165.}
                                UNION
                                { ?place wdt:P31 wd:Q4022.}
                                UNION
                                { ?place wdt:P31 wd:Q3253281.}
                                UNION
                                { ?place wdt:P31 wd:Q43742.}
                                UNION
                                { ?place wdt:P31 wd:Q693340.}
                                UNION
                                { ?place wdt:P31 wd:Q44782.}
                                UNION
                                { ?place wdt:P31 wd:Q1377943.}
                                UNION
                                { ?place wdt:P31 wd:Q23397.}
                                UNION
                                { ?place wdt:P31 wd:Q3215290.}
                                UNION
                                { ?place wdt:P31 wd:Q215635.}
                                UNION
                                { ?place wdt:P31 wd:Q656586.}
                                UNION
                                { ?place wdt:P31 wd:Q15324.}
                                UNION
                                { ?place wdt:P31 wd:Q107679.}
                                UNION
                                { ?place wdt:P31 wd:Q33837.}
                                UNION
                                { ?place wdt:P31 wd:Q39594.}
                                UNION
                                { ?place wdt:P31 wd:Q350495.}
                                UNION
                                { ?place wdt:P31 wd:Q2897058.}
                                UNION
                                { ?place wdt:P31 wd:Q899514.}
                                UNION
                                { ?place wdt:P31 wd:Q1026259.}
                                UNION
                                { ?place wdt:P31 wd:Q34038.}
                                UNION
                                { ?place wdt:P31 wd:Q1210950.}
                                UNION
                                { ?place wdt:P31 wd:Q1233637.}
                                UNION
                                { ?place wdt:P31 wd:Q31615.}
                                UNION
                                { ?place wdt:P31 wd:Q25391.}
                                UNION
                                { ?place wdt:P31 wd:Q40080.}
                                UNION
                                { ?place wdt:P31 wd:Q29925.}
                                UNION
                                { ?place wdt:P31 wd:Q124714.}
                                UNION
                                { ?place wdt:P31 wd:Q47053.}
                                UNION
                                { ?place wdt:P31 wd:Q192503.}
                                UNION
                                { ?place wdt:P31 wd:Q30198.}
                                UNION
                                { ?place wdt:P31 wd:Q355304.}
                                UNION
                                { ?place wdt:P31 wd:Q30198.}
                                UNION
                                { ?place wdt:P31 wd:Q721207.}
                                UNION
                                { ?place wdt:P31 wd:Q187971.}
                                UNION
                                { ?place wdt:P31 wd:Q185113.}
                                UNION
                                { ?place wdt:P31 wd:Q1681353.}
                                UNION
                                { ?place wdt:P31 wd:Q207524.}
                                UNION
                                { ?place wdt:P31 wd:Q93267.}
                                UNION
                                { ?place wdt:P31 wd:Q2578218.}
                                UNION
                                { ?place wdt:P31 wd:Q1172599.}
                                UNION
                                { ?place wdt:P31 wd:Q2362867.}
                                UNION
                                { ?place wdt:P31 wd:Q913035.}
                                UNION
                                { ?place wdt:P31 wd:Q573344.}
                                UNION
                                { ?place wdt:P31 wd:Q30198.}
                                UNION
                                { ?place wdt:P31 wd:Q124282.}
                                UNION
                                { ?place wdt:P31 wd:Q159675.}
                                UNION
                                { ?place wdt:P31 wd:Q106259.}
                                UNION
                                { ?place wdt:P31 wd:Q946033.}
                                UNION
                                { ?place wdt:P31 wd:Q1424609.}
                                UNION
                                { ?place wdt:P31 wd:Q1245089.}
                                UNION
                                { ?place wdt:P31 wd:Q187223.}
                                UNION
                                { ?place wdt:P31 wd:Q747957.}
                                UNION
                                { ?place wdt:P31 wd:Q105731.}
                                UNION
                                { ?place wdt:P31 wd:Q12280.}
                                UNION
                                { ?place wdt:P31 wd:Q23442.}"""


        }

        if list_dict:
            world = list_dict.split(' ')
            params = ''
            for value in world:
                params += a[value]
                if value == world[-1]:
                    break
                params += '\n' 'UNION'
        else:
            params = ''

        queryall = """
                     SELECT DISTINCT ?placeLabel ?place ?location ?distance ?image ?article WHERE { 
                     SERVICE wikibase:around { 
                         ?place wdt:P625 ?location. 
                         bd:serviceParam wikibase:center "Point(""" + str(longitude) + " " + str(latitude) + """)"^^geo:wktLiteral . 
                         bd:serviceParam wikibase:radius """ + str(radius) + """ .
                         bd:serviceParam wikibase:distance ?distance .
                       }
                       """ + str(params) + """
                       ?article schema:about ?place ;
                                 schema:isPartOf <https://""" + str(languages) + """.wikipedia.org/> .
                       SERVICE wikibase:label { bd:serviceParam wikibase:language " en,""" + str(languages) + """[AUTO_LANGUAGE]". }
                       OPTIONAL { ?place wdt:P18 ?image. }
                     }}"""

        celeb1 = """   
                     SELECT DISTINCT ?location ?distance ?person ?personLabel ?image ?article WHERE {
                         SERVICE wikibase:around { 
                             ?place wdt:P625 ?location .
                              bd:serviceParam wikibase:center "Point(""" + str(longitude) + " " + str(latitude) + """)"^^geo:wktLiteral . 
                              bd:serviceParam wikibase:radius """ + str(radius) + """ . 
                              bd:serviceParam wikibase:distance ?distance .
                         } 
                     ?person wdt:P19 ?place .
                     ?article schema:about ?person ;
                             schema:isPartOf <https://""" + str(languages) + """.wikipedia.org/> .
                         SERVICE wikibase:label { bd:serviceParam wikibase:language "en,""" + str(languages) + """[AUTO_LANGUAGE]". }
                     OPTIONAL{?person wdt:P18 ?image}
                     }}
                     """

        celeb = ""

        if celebrite:
            celeb = celeb1
            if len(list_dict) == 0:
                queryall = ''
            if len(list_dict) >= 1 and len(celebrite) >= 1:
                celeb = "UNION{" + celeb1

        if len(celebrite) == 0 and len(list_dict) == 0:
            celeb = "UNION{" + celeb1
        # for thematics make "" between each thematic and split with a space for the front-end

        endpoint_url = "https://query.wikidata.org/sparql"

        # this is the query for wikidata "https://query.wikidata.org/"
        # query for find monument around longitude and latitude and radius for limit the distance
        # params = the user choice thematics
        # languages = fr or en
        # pages optional for now but later for split monuments in few pages

        query = """SELECT * {{
         """ + queryall + """
         """ + celeb + """
        }
        ORDER BY ?distance
        LIMIT 20 OFFSET """ + str(pages) + """"""

        # this method help to convert the query into Json
        #print(query)
        def get_results(endpoint_url, query):
            user_agent = "WDQS-example Python/%s.%s" % (sys.version_info[0], sys.version_info[1])
            # TODO adjust user agent; see https://w.wiki/CX6
            sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
            sparql.setQuery(query)
            sparql.setReturnFormat(JSON)
            return sparql.query().convert()

        # get the result
        results = get_results(endpoint_url, query)
        item = []

        # for 1 item on results create a different item
        for result in results["results"]["bindings"]:

            """categorie = result['place']['value']
            per = categorie.replace('http://www.wikidata.org/entity/', '')
            per1 = "https://www.wikidata.org/wiki/" + per
            pr = requests.get(per1)
            soup1 = BeautifulSoup(pr.text, "html.parser")
            tio = soup1.find("div", class_="wikibase-labelview")"""

            # under the commentary the function is for scrap the reel title of the place because in the query some
            # title are not defined

            article = result['article']['value']
            # t = any(map(str.isdigit, title))
            # if t is True:
            rsp = requests.get(article)
            soup = BeautifulSoup(rsp.text, "html.parser")
            per = soup.h1
            titl = per.text

            if 'image' in result:
                image = result['image']['value']
                img = image.replace('http', 'https')
            else:
                img = None
            # 1 item need all of that from the query expect the image is optional
            if 'person' in result:
                id = result['person']['value']
            else:
                id = result['place']['value']

            item.append({
                "id": id,
                "title": titl,
                "distance": result['distance']['value'],
                "article": result['article']['value'],
                "_links": {
                    "image": {
                        'self': img,
                    }
                },
            })

        # return a json&
        # number = len(results['results']["bindings"])
        # return str(number)

        return jsonify(item)
