from flask import json, request
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time
from model.modelgeosearch import REPORTS_QUERY
from utils.request_verifier import verify_dict_body
#from flask_mail import Mail, Message
from flask import current_app


class Errors:
    def report_mail(self):
        validation = verify_dict_body(request.json, REPORTS_QUERY)
        if validation is not None:
            return validation

        # method post and params need email,message,os_version,language
        if request.method == 'POST':
            titre = request.json.get('title')
            message = request.json.get('message')
            date_heure = request.json.get('date_heure')
            url = request.json.get('url')

        # return a json
        commentaire = "lien de l'article en question =" + url + "\n" + message + "\n" + "message éditée à " + date_heure

        msg = Message(titre,
                      sender=current_app.config.get("MAIL_USERNAME"),
                      recipients=['WikiwayError@gmail.com'])
        msg.body = commentaire
        current_app.mail.send(msg)

        return 'message envoyé'

    def post_report(self):

        # for verify if the params is true
        validation = verify_dict_body(request.json, REPORTS_QUERY)
        if validation is not None:
            return validation

        # method post and params need email,message,os_version,language
        if request.method == 'POST':
            titre = request.json.get('title')
            message = request.json.get('message')
            date_heure = request.json.get('date_heure')
            url = request.json.get('url')

        # return a json
        commentaire = "== " + titre + " édité le " + date_heure + " ==" + "\n" + url + "\n" + message
        # This is a sample Python script.

        try:

            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--window-size=1420,1080')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            driver = webdriver.Chrome(options=chrome_options)

            # s = Service('/usr/bin/chromedriver' options=options) # for mac server '/usr/local/bin/chromedriver' for linux  "/usr/bin/chromedriver"
            # browser = webdriver.Chrome(service=s)
            url = 'https://fr.wikipedia.org/w/index.php?title=Projet:Wikiway/Commentaires&action=edit'
            driver.get(url)
            time.sleep(2)
            main = driver.find_element(By.ID, 'wpTextbox1')
            driver.find_element(By.LINK_TEXT, "Commencer à modifier").send_keys('\n')
            # browser.execute_script('document.getElementsByName("oo-ui-buttonElement-button")[0].click();')
            print(driver.title)
            # browser.back()

            main.send_keys(commentaire)
            time.sleep(2)
            content = driver.find_element(By.ID, 'wpPreview').click()

            content2 = driver.find_element(By.ID, 'wpSave').click()
            driver.quit()

            return "votre message a été envoyer"

        except:
            return commentaire + "réesayer"
