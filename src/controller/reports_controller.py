from flask import json, request
from model.modelgeosearch import REPORTS_QUERY
from utils.request_verifier import verify_dict_body


class Reports:
    """class Reports: class allow user to create a message for error in the app
            return on json : the message of the user"""

    def post_repost(self):
        # for verify if the params is true
        validation = verify_dict_body(request.json, REPORTS_QUERY)
        if validation is not None:
            return validation

        # method post and params need email,message,os_version,language
        if request.method == 'POST':
            email = request.json.get('email')
            message = request.json.get('message')
            os_version = request.json.get('os_version')
            language = request.json.get('language')

        # return a json
            return json.dumps(
                {'email': email,
                 'message': message,
                 'os_version': os_version,
                 'language': language}
            )
