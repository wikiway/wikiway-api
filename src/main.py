import configparser
from flask import Flask
# from waitress import serve
from controller.errors_controller import Errors
from controller.pages_controller import Geosearch
from controller.reports_controller import Reports
from model.error.errors import internal_error, not_found, method_not_allowed
from utils.html_parser import HtmlParser
from config import Config

# from flask_mail import Mail

configu = Config()
# config the route
app = Flask(__name__)
app.config.from_object(configu)
# mail = Mail(app)
# app.mail = mail
# init config from config file
parser = configparser.ConfigParser()
parser.read("../resources/config.ini")
conf_dict = {section: dict(parser.items(section)) for section in parser.sections()}

# init all controllers
geosearch = Geosearch()
reports = Reports()
htmlparse = HtmlParser()
errorsapi = Errors()

# endpoints api
app.add_url_rule(rule="/pages", view_func=geosearch.get_geosearch)

app.add_url_rule(rule="/reports", view_func=reports.post_repost, methods=['POST'])

app.add_url_rule(rule="/errors", view_func=errorsapi.report_mail, methods=['POST'])

app.add_url_rule(rule="/html_parse", view_func=htmlparse.get_result)


# errorhandler
@app.errorhandler(404)
def resource_not_found(e):
    return not_found()


@app.errorhandler(500)
def unknown_error(e):
    return internal_error()


@app.errorhandler(405)
def notallowed(e):
    return method_not_allowed()


# start the app
# the default port is 5000
if __name__ == '__main__':
    # serve(app, host='127.0.0.1', port=5000)
    app.run(host='127.0.0.1', port=5000)
